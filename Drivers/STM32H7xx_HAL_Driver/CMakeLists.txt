cmake_minimum_required(VERSION 3.22)

#target_sources
add_library(HAL
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_cortex.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_dma_ex.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_dma.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_exti.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_flash_ex.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_flash.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_gpio.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_hsem.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_i2c_ex.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_i2c.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_mdma.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_pwr_ex.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_rcc_ex.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_rcc.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_tim_ex.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_tim.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_uart_ex.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal_uart.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Src/stm32h7xx_hal.c
)


target_include_directories(HAL 
    INTERFACE 
        ${CMAKE_CURRENT_SOURCE_DIR}/Inc
)
