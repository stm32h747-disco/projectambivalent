cmake_minimum_required(VERSION 3.22)

target_sources(THREAD_SAFE
    PRIVATE
        newlib_lock_glue.c
        stm32_lock.h
)

target_include_directories(myLib PUBLIC ${CMAKE_CURRENT_LIST_DIR})
