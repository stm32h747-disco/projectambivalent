/*
 * sysio.c
 *
 *  Created on: May 26, 2022
 *      Author: etien
 */

#include <usart.h>

int __io_putchar(int ch)
{
	return HAL_UART_Transmit(&huart1, (const uint8_t *)&ch, 1, 10);
}

int __io_getchar(void)
{
	int ch = 0;

	HAL_UART_Receive(&huart1, (uint8_t *)&ch, 1, HAL_MAX_DELAY);
	return ch;
}
